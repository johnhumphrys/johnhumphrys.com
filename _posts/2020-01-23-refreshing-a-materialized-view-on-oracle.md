---
layout: post
title: 📦 Refreshing a materialized view on Oracle
date: 2020-01-23 00:00
categories: database
---

## Overview

A materialized view is a database object that contains the results of a query. For example, it may be a local copy of data located remotely, or may be a subset of the rows and/or columns of a table or join result, or may be a summary using an aggregate function. 

These are the two ways we have been refreshing the views with the mv as the param:


{% highlight sql lineanchors %}
begin DBMS_REFRESH.REFRESH('T2_VIRTUAL_SQUADS_mv'); end;
{% endhighlight %}

These are the following refresh methods, I've always used COMPLETE as a developer and it works for me.

![Refresh Methods](/images/misc/mv-refresh-methods.PNG){:class="img-responsive"}

{% highlight sql %}
BEGIN DBMS_SNAPSHOT.REFRESH( 'SCOUT.benchmarks_periods_mv','C'); end;
{% endhighlight %}

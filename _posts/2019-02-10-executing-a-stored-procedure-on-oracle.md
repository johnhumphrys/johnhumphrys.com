---
layout: post
title: 📦 Executing a Stored Procedure on Oracle
date: 2019-02-10 19:22
categories: database
---

## Overview

A stored procedure has a number of parameters, each one is either an IN or OUT.

When executing a stored procedure, for each OUT parameter, a variable is required. Oracle will write the outputted values to these variables, which can be used later.

This example will simply output them to the screen with DBMS.

The following is an example with only OUT params:

{% highlight sql lineanchors %}
PROCEDURE LoadFixtureFromDB (
p_RetVal OUT NUMBER,
p_ErrorCode OUT NUMBER,
p_ErrorDesc OUT VARCHAR2
);
{% endhighlight %}

In order to run this and output to the console, you can use the following code:

{% highlight sql %}
DECLARE
nRET NUMBER;
nCODE NUMBER;
vDESC VARCHAR2(300);

BEGIN
UTILITIES_API.LoadFixtureFromDB (
nRET,
nCODE,
vDESC
);

DBMS_OUTPUT.PUT_LINE( 'Ret ' || nRET || ', Code ' || nCODE || ', Desc ' || vDESC);

END;
{% endhighlight %}

When the procedure requires a IN value, you can simply supply it in the function call.

{% highlight sql lineanchors %}
PROCEDURE LoadFixtureFromDB (
p_SeasonId IN NUMBER,
p_RetVal OUT NUMBER,
p_ErrorCode OUT NUMBER,
p_ErrorDesc OUT VARCHAR2
);
{% endhighlight %}

{% highlight sql %}
DECLARE
nRET NUMBER;
nCODE NUMBER;
vDESC VARCHAR2(300);

BEGIN
UTILITIES_API.LoadFixtureFromDB (
2019,
nRET,
nCODE,
vDESC
);

DBMS_OUTPUT.PUT_LINE( 'Ret ' || nRET || ', Code ' || nCODE || ', Desc ' || vDESC);

END;
{% endhighlight %}

---
layout: post
title: 🔁 Bash looping
date: 2019-02-20 19:22
categories: bash
---

## Overview

When using bash to automate basic tasks, for loops can take a lot of the manual work out of it

Here are a couple of examples when I often use them:

{% highlight bash %}

// Will substitute each value into $i
for i in 104100101 104100102 104100103 104100104; do ./doAction.sh -e events/AFL_misc/visionTrxs.groovy -f $i; done

{% endhighlight %}

{% highlight bash %}

// Will run through the file and substitute each value into $i
for i in `cat fixedIds/afl2017homeaway`; do ./doAction.sh -e events/AFL_misc/visionTrxs.groovy -f $i; done

{% endhighlight %}

Easy!

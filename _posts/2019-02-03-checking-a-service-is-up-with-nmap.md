---
layout: post
title: 👀 Checking a service is up with bash & nmap
date: 2019-02-03 19:22
categories: bash devops
---

## Overview

Adding a simple check to see if a service can be useful while creating a CI/CD pipeline for a legacy application.

We have bash scripts written for starting and stopping our jetty application.

This small snippet simply loops and checks for the application port to open, it only fails when the pipeline timeout occurs, which is a low threshold.

{% highlight bash %}

// define this elsewhere with your own port
jettyPort=8080

while :
do
/usr/bin/nmap -p ${jettyPort} localhost | grep -q " open " && {
        /bin/echo "Listening on port ${jettyPort}"
break
}
/bin/sleep 1
done
{% endhighlight %}

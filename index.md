---
layout: page
permalink: /
---

Hi, I'm John, a Software Developer in Melbourne, Australia.

I am experienced in a number of languages including React, Node.js & Java and also skilled in devops and infrastructure.

I'm interested in the research and implementation of IoT in environmental and sustainable settings.

In my personal time, I enjoy working on my personal projects, building my project car or go hiking.
